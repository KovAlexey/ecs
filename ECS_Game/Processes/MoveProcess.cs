﻿using System;
using System.Collections.Generic;
using ECS_Game.Base;
using ECS_Game.Node;

namespace ECS_Game.Processes
{
    public class MoveProcess : CProcess
    {
             
        public new string NodeName = NodeNameList.MoveNodeName;
        public MoveProcess(List<CNode> nodeList) : base(ProcessPrioritys.MidPriority, nodeList)
        {
            
        }

        public override void Start()
        {
            
        }


        public override void Stop()
        {
            
        }

        public override void Update()
        {
            foreach (MoveNode node in _nodeList)
            {
                node.Position.x += node.Move.MoveX;
                node.Position.y += node.Move.MoveY;
            }
        }
    }
}