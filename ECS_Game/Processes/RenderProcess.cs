﻿using System;
using System.Collections.Generic;
using ECS_Game.Base;
using ECS_Game.Node;
using SharpDX.Direct3D;
using SharpDX.DXGI;
using SharpDX.Mathematics.Interop;
using D3D11 = SharpDX.Direct3D11;
using SharpDX.Windows;

namespace ECS_Game.Processes
{
    public class RenderProcess : CProcess
    {
        private IntPtr _renderFormHandle;
        private int _width;
        private int _height;
        private D3D11.Device d3dDevice;
        private D3D11.DeviceContext d3dDeviceContext;
        private SwapChain swapChain;
        private D3D11.RenderTargetView renderTargetView;

        public RenderProcess(List<CNode> nodeList, int width, int height, IntPtr handle) : base(ProcessPrioritys.LowestPrirority, nodeList)
        {
            _renderFormHandle = handle;
            _width = width;
            _height = height;
        }

        public override void Start()
        {
            InitializeDeviceResources();
        }

        public override void Update()
        {
            Draw();
        }

        public override void Stop()
        {
            
        }

        private void InitializeDeviceResources()
        {
            ModeDescription modeDescription = new ModeDescription(_width, _height, new Rational(60, 1), Format.R8G8B8A8_UNorm);

            SwapChainDescription swapChainDescription = new SwapChainDescription()
            {
                ModeDescription = modeDescription,
                BufferCount = 1,
                IsWindowed = true,
                OutputHandle = _renderFormHandle,
                Usage = Usage.RenderTargetOutput,
                SampleDescription = new SampleDescription(1, 0)
            };
            D3D11.Device.CreateWithSwapChain(DriverType.Hardware, D3D11.DeviceCreationFlags.None, 
                swapChainDescription, out d3dDevice, out swapChain);
            d3dDeviceContext = d3dDevice.ImmediateContext;

            using (D3D11.Texture2D backBuffer = swapChain.GetBackBuffer<D3D11.Texture2D>(0))
            {
                renderTargetView = new D3D11.RenderTargetView(d3dDevice, backBuffer);
            }

            d3dDeviceContext.OutputMerger.SetRenderTargets(renderTargetView);
        }

        private void Draw()
        {
            d3dDeviceContext.ClearRenderTargetView(renderTargetView, new RawColor4(100,100,0,255));
            swapChain.Present(1, PresentFlags.None);
        }
    }
}