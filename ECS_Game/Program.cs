﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECS_Game.Base;
using ECS_Game.Components;
using ECS_Game.Node;
using ECS_Game.Processes;
using SharpDX.Windows;

namespace ECS_Game
{
    class Program
    {
        private static ProcessManager processManager;
        static void Main(string[] args)
        {
            processManager  = new ProcessManager();
            Entity newEntity = new Entity();
            PositionComponent position = new PositionComponent();
            position.x = 10;
            position.y = 20;
            MoveComponent moveComponent = new MoveComponent();
            moveComponent.MoveX = 0;
            moveComponent.MoveY = 3;
            newEntity.AddComponent(position);
            newEntity.AddComponent(moveComponent);
            processManager.AddEntity(newEntity);
            MoveProcess moveProcess = new MoveProcess(processManager.GetNodeList(NodeNameList.MoveNodeName));
            processManager.AddProcess((CProcess)moveProcess);
            RenderForm renderForm = new RenderForm("SharpDX Window");
            renderForm.ClientSize = new Size(1000, 600);
            renderForm.AllowUserResizing = false;
            RenderProcess renderProcess = new RenderProcess(new List<CNode>(), 1000, 600, renderForm.Handle);
            processManager.AddProcess(renderProcess);
            RenderLoop.Run(renderForm, RenderCallback);
        }

        private static void RenderCallback()
        {
            processManager.Update();
        }
    }
}
