﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using ECS_Game.Components;

namespace ECS_Game.Base
{
    public class Entity
    {
        private Dictionary<string, Component> componentsList;

        public Entity()
        {
            componentsList = new Dictionary<string, Component>();
        }

        public List<string> GetNodesNames()
        {
            List<string> nodeList = new List<string>();

            List<string> componentNamesList = componentsList.Keys.ToList();
            if (componentNamesList.Contains(ComponentTypes.MoveComponentName))
            {
                nodeList.Add(NodeNameList.MoveNodeName);
            }

            return nodeList;
        }

        public Component GetComponent(string name)
        {
            if (componentsList.ContainsKey(name))
            {
                return componentsList[name];
            }
            else
            {
                return null;
            }
        }

        public void AddComponent(Component component)
        {
            componentsList.Add(component.ComponentType, component);
        }
    }
}