﻿using ECS_Game.Node;
using System;
using System.Collections.Generic;

namespace ECS_Game.Base
{


    public abstract class CProcess
    {
        public int Priority;
        public string NodeName = "CProcess";
        protected List<CNode> _nodeList;

        protected CProcess(int priority, List<CNode> nodeList)
        {
            _nodeList = nodeList;
            Priority = priority;
        }


        public abstract void Start();
        public abstract void Update();
        public abstract void Stop();
    }
}


namespace ECS_Game
{
    public struct ProcessPrioritys
    {
        public const int HighestPriority = 0;
        public const int HighPriority = 1;
        public const int MidPriority = 2;
        public const int LowPriority = 3;
        public const int LowestPrirority = 4;
    }
}