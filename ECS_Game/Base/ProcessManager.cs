﻿using System.Collections.Generic;
using System.Collections.Specialized;
using ECS_Game.Components;
using ECS_Game.Node;

namespace ECS_Game.Base
{
    public class ProcessManager
    {
        private List<List<CProcess>> PriorityProcessesList;
        private List<CProcess> ProcessAddQueue;        
        private List<Entity> entitys;
        private Dictionary<string, List<CNode>> nodeList;

        private bool _running = false;
        private bool _haveNewProcess = false;

        public ProcessManager()
        {
            PriorityProcessesList = new List<List<CProcess>>();
            ProcessAddQueue = new List<CProcess>();
            nodeList = new Dictionary<string, List<CNode>>();
            entitys = new List<Entity>();
            for (int i = 0; i <= ProcessPrioritys.LowestPrirority; i++)
            {
                PriorityProcessesList.Add(new List<CProcess>());
            }
        }

        private void AddNode(string nodeName, CNode node)
        {
            if (!nodeList.ContainsKey(nodeName))
            {
                nodeList.Add(nodeName, new List<CNode>());
            }
            nodeList[nodeName].Add(node);
        }

        public List<CNode> GetNodeList(string nodeName)
        {
            List<CNode> node = null;
            if (nodeList.ContainsKey(nodeName))
            {
                node = nodeList[nodeName];
            }
            else
            {
                nodeList.Add(nodeName, new List<CNode>());
            }
            return node;
        }

        public void AddEntity(Entity entity)
        {
            List<string> nodeListNames = entity.GetNodesNames();

            if (nodeListNames.Contains(NodeNameList.MoveNodeName))
            {
                MoveNode moveNode = new MoveNode();
                moveNode.Move = (MoveComponent) entity.GetComponent(ComponentTypes.MoveComponentName);
                moveNode.Position = (PositionComponent) entity.GetComponent(ComponentTypes.PositionComponentName);
                AddNode(NodeNameList.MoveNodeName, moveNode);
            }
            entitys.Add(entity);
        }

        public void AddProcess(CProcess process) 
        {
            if (!_running)
            {
                PriorityProcessesList[process.Priority].Add(process);
                process.Start();
            }
            else
            {
                //Добавляем процесс в очередь на "добавление"
                ProcessAddQueue.Add(process);
                _haveNewProcess = true;
            }
        }

        private void UpdateProcesses()
        {
            foreach (var process in ProcessAddQueue)
            {
                PriorityProcessesList[process.Priority].Add(process);
                process.Start();
            }
            _haveNewProcess = false;
        }

        public void Update()
        {
            _running = true;
            foreach (var processList in PriorityProcessesList)
            {
                foreach (var process in processList)
                {
                    process.Update();
                }
            }

            if (_haveNewProcess) //Если появились новые процессы, добавляем их
            {
                UpdateProcesses();
            }
            _running = false;
        }
    }
}