﻿namespace ECS_Game.Components
{
    public class MoveComponent : Component
    {
        public float MoveX, MoveY;

        public MoveComponent() : base(ComponentTypes.MoveComponentName)
        {
        }
    }
}