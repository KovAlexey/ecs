﻿namespace ECS_Game.Components
{
    public class PositionComponent : Component
    {
        public float x, y;

        public PositionComponent() : base(ComponentTypes.PositionComponentName)
        {
        }

        
    }
}