﻿using ECS_Game.Components;

namespace ECS_Game.Components
{
    public abstract class Component
    {
        public string ComponentType { get; private set; }

        protected Component(string type)
        {
            ComponentType = type;
        }
    }
}

namespace ECS_Game
{
    public class ComponentTypes
    {
        public const string MoveComponentName = "moveComponent";
        public const string PositionComponentName = "positionComponent";
    }
}