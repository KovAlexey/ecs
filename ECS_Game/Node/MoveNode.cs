﻿using ECS_Game.Components;

namespace ECS_Game.Node
{
    public class MoveNode : CNode
    {
        public PositionComponent Position;
        public MoveComponent Move;
    }
}